# TheWeatherApp
Simple App written in Java to check the weather for any given city through internet connection, it uses OpenWeatherMap.org API
https://openweathermap.org/api

The app obviously needs internet connection, the user enters the city name and gets weather details back like:

1-Date
2-Temperature in Celsius
3-An icon that shows the current state of the weather
4-Weather State Like "Clear Sky"
5-Humidity
6-Pressure
7-Wind Speed

The improvised app was based on this tutorial 
https://androstock.com/tutorials/create-a-weather-app-on-android-android-studio.html
Modifications include changes to the user interface as well as the addition of wind speed.
