package com.appcreator.isa.theweatherapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    Button SearchCityButton;

    EditText EnterCityNameEditText;

    TextView CityText;
    TextView DetailsText;
    TextView TemperatureText;
    TextView HumidityText;
    TextView PressureText;
    TextView WindSpeedText;
    TextView WeatherIcon;
    TextView DateTimeText;

    ProgressBar loader;
    Typeface weatherFront;

    String city = "";

    String OPEN_WEATHER_MAP_API = "62ee3fd8cff0ba8a9ff9f644dc79fb7c";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loader = (ProgressBar) findViewById(R.id.loader);

        SearchCityButton = (Button) findViewById(R.id.searchCityButton);

        EnterCityNameEditText = (EditText)findViewById(R.id.enter_city_edit_text);

        CityText = (TextView) findViewById(R.id.city_text);
        DateTimeText = (TextView) findViewById(R.id.date_text);
        DetailsText = (TextView) findViewById(R.id.details_text);
        TemperatureText = (TextView) findViewById(R.id.temperature_text);
        HumidityText = (TextView) findViewById(R.id.humidity_text);
        PressureText = (TextView) findViewById(R.id.pressure_text);
        WindSpeedText = (TextView)findViewById(R.id.windspeed_text);
        WeatherIcon = (TextView) findViewById(R.id.weather_icon);

        weatherFront = Typeface.createFromAsset(getAssets(), "fonts/weathericons-regular-webfont.ttf");
        WeatherIcon.setTypeface(weatherFront);

         taskLoadUp(city);

       SearchCityButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                city = EnterCityNameEditText.getText().toString();
                taskLoadUp(city);
            }

        });

    }

    public void taskLoadUp(String query)
    {
        if (WeatherFunction.NetworkStatus(getApplicationContext()))
        {
            DownloadWeather task = new DownloadWeather();
            task.execute(query);
        }

        else
        {
            Toast.makeText(getApplicationContext(), "Could not retrieve weather data !, please check your internet connection and try again !", Toast.LENGTH_LONG).show();
        }
    }
    class DownloadWeather extends AsyncTask < String, Void, String >
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            loader.setVisibility(View.VISIBLE);
        }

        protected String doInBackground(String...args)
        {
            String xml = WeatherFunction.startGet("http://api.openweathermap.org/data/2.5/weather?q=" + args[0] +
                    "&units=metric&appid=" + OPEN_WEATHER_MAP_API);
            return xml;
        }

        @Override
        protected void onPostExecute(String xml)
        {

            try
            {
                JSONObject json = new JSONObject(xml);
                if (json != null)
                {
                    JSONObject details = json.getJSONArray("weather").getJSONObject(0);
                    JSONObject main = json.getJSONObject("main");
                    JSONObject wind = json.getJSONObject("wind");
                    DateFormat df = DateFormat.getDateTimeInstance();

                    CityText.setText(json.getString("name").toUpperCase(Locale.US) + ", " + json.getJSONObject("sys").getString("country"));
                    DetailsText.setText(details.getString("description").toUpperCase(Locale.US));
                    TemperatureText.setText(String.format("%.2f", main.getDouble("temp")) + "°");
                    HumidityText.setText("Humidity: " + main.getString("humidity") + "%");
                    PressureText.setText("Pressure: " + main.getString("pressure") + " hPa");
                    WindSpeedText.setText("Wind Speed: " + wind.getString("speed") + " m/s");
                    DateTimeText.setText(df.format(new Date(json.getLong("dt") * 1000)));
                    WeatherIcon.setText(Html.fromHtml(WeatherFunction.SetWeatherIcon(details.getInt("id"),
                            json.getJSONObject("sys").getLong("sunrise") * 1000,
                            json.getJSONObject("sys").getLong("sunset") * 1000)));

                    loader.setVisibility(View.GONE);

                }
            }
            catch (JSONException e)
            {
                Toast.makeText(getApplicationContext(), "Error, Make Sure you entered the city name correctly !", Toast.LENGTH_SHORT).show();
            }


        }



    }

}
